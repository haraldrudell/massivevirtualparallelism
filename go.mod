module codeberg.org/haraldrudell/massivevirtualparallelism

go 1.18

require github.com/haraldrudell/parl v0.2.2

require golang.org/x/text v0.3.7
