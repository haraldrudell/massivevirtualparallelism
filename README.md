# massivevirtualparallelism

# 15 million virtual threads — averaging 600,000 new per second
This program tests how many threads **Go** can run in practice

There is no apparent limit to the number of threads other than available swap space, time and the size of int

On a 2021 Apple MacBook Pro 64 GiB 32 gpu, **Go** can without swap on a 64 GiB computer launch 15 million threads at a rate of 600,000 per second. Threads consume about 3 kilobytes a piece.
* Running **Go** with many virtual threads is memory limited at 3 KiB per thread
* Running without using swap (2/3 of memory) on 64 GiB is 15 million threads launching at 600,000 threads per second at 41 GiB memory usage completing in about 30 seconds
* 50 million threads completes in about 10 minutes at 140 GiB memory usage
* At 85 million threads and 4 times overcommit, the computer is thrashing
* 100 million threads completes in 70 minutes: ```go run ./ 1e8```
* There is no apparent limit to the number of threads other than available swap space and the size of int
  * In particular, **Go** produces no errors or panics
* **Go** is processor usage per top is in the 600% to 980% range at 10% idle for 8+2 cores per ```top -ocpu```
  * **Go** is processor limited
* **Go** and swap together consumes about 900% processor cores with 10% idle
* See the file 50million.txt and 15million.txt: ```go run ./ 5e7 2>&1 | tee 50million.txt```

<br />
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)<br />
<a href=./LICENSE>ISC License</a>
<br />
<br />
<img src=./assets/theflag.png>
“justice, peace and massive virtual parallelism”
<br />
<br />

```
go run ./ 1e8

massivevirtualparallelism 2022-03-21 14:20:34-07:00
host: c66 go1.18 os-arch: darwin arm64 64 GiB cores: 10
Launching threads: 100,000,000…
265μs Status thread count: 100. Waiting for thread launch to complete…
1.0s running: 805,898 launched: 805,898 launch-rate: 771,632/s started: 804,929 start-rate: 770,708/s
…
4.1s running: 2,892,619 launched: 2,892,619 launch-rate: 272,506/s started: 2,892,600 start-rate: 272,505/s
6.3s EXECUTION DELAY: 2.1s expected: 1.0s
23.3s EXECUTION DELAY: 10.3s expected: 1.0s
1m45s EXECUTION DELAY: 31.2s expected: 1.0s
3m4s EXECUTION DELAY: 1m19s expected: 1.0s
…
5m4s Thread launch complete
5m4s Waiting 3s s for ticker events
5m4s running: 100,000,000 launched: 100,000,000 launch-rate: 270,944/s started: 100,000,000 start-rate: 273,236/s
…
5m7s Shutting down…
5m7s running: 99,987,400 exited: 12,600 exit-rate: 12,600/s
…
1h9m9s Thread shutdown complete: Maximum time between ticks (normally 1 second): 1m19s
1h9m9s Maxima observed: max duration: 3m29s max value: 100,000,000 max rate: 1,308,930
1h9m9s running: 0 ticks: 218 launched: 100,000,000 started: 100,000,000 exited: 100,000,000 panic: 0 errs: none
1h9m9s massivevirtualparallelism completed successfully
```

## How to use
* go get codeberg.org/haraldrudell/massivevirtualparallelism
* Download and build the Go project from <a href=https://codeberg.org/haraldrudell/massivevirtualparallelism>codeberg.org</a>
* Download the executable for your platform (macOS will not immediately execute)
  * <a href=https://codeberg.org/haraldrudell/massivevirtualparallelism/raw/branch/main/bin/darwin-arm64/massivevirtualparallelism>macOS M1</a>
  *  <a href=https://codeberg.org/haraldrudell/massivevirtualparallelism/raw/branch/main/bin/darwin-amd64/massivevirtualparallelism>macOS Intel</a>
  * <a href=https://codeberg.org/haraldrudell/massivevirtualparallelism/raw/branch/main/bin/linux-amd64/massivevirtualparallelism>Linux amd64</a>

## Documentation

<a href="https://pkg.go.dev/codeberg.org/haraldrudell/massivevirtualparallelism"><img src="https://pkg.go.dev/badge/github.com/haraldrudell/parl.svg" alt="Go Reference">&emsp; Documentation in the Go Package Index</a>
<br />
<br />
On March 19, 2022, massivevirtualparallelism was open-sourced under an ISC License<br />
<br />
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
