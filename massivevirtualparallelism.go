/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License

go run ./
*/

/*
massivevirtualparallelism tests how many goroutines can be run in practice

Virtual Threads

What sets Go apart is the concept of virtual threads. Other languages have had similar concepts
but not like Go with machine-instruction synchronization mechanics and runtime support hiding
blocking

Virutal threads is a necessity going forward because there is not adequate hardware support in
terms of processor cores and operating system threads to respond in a performant way. Polling
and event queues is not performant. With virtual threads, the programmer can define millions of
activities and possible events to be urgent

Virtual threads is how to effectively utilize the processors, cores and hyper threads offered by
the hardware in 2022

Because virtual threads is difficult to retrofit, it was the right decision by the Pike to create a
new language. Either the runtime has to be re-written or the programmer becomes subject
to draconian restictions. Oracle has a Java Loom project to rewrite the Java runtime for virtual
threads

Other Go Benefits

Go also has a practically pause-less garbage collector, the ability to utilize native platform
interfaces easily and the concept of contexts allowing all possible actions to be started immediately
and subsequently cancelling those circumstances do not call for. This is the fastest way to obtain
results, allows encapsulated threads and avoids complex and error prone execution graphs

The Go ability to cross compile, support many architectures and operating systems and deploy as
a self-contained executable brings comforting portability with machine-code performance

Massive Parallelism

Massive parallelism is a system with hundreds or thousands of processor cores allowing for a large
number of tasks to execute in parallel. The related concept concurrency is perceived parallelism by
access to processing resources shared over time. Go is not usually massively parallel but still
parallel on four or so processor cores

Parallelsim is important because the historically single-threaded Node.js can lock up from a single
mistake, while Go would still have additional parallel threads to continue execution and the ability to
schedule a different virtual thread for the operating system thread. An event queue is
troublesome for high-traffic servers, because it introduces delays in processing of incoming
requests. Most scripted languages are single-threaded by a global interpreter lock. Besides Go,
the good options are very few

Multi-threaded programming can be diffcult and fault prone. In Go, Rob Pike has simplified mercilessly
to only support one synchornization mechanic in the Go memory model: the lock. Because a thread
may have dozens of locks for different things, those locks are low-contention. Pike no like atomics

Conclusion — 50 million virtual threads — averaging 240,000 launched per second

On a 2021 Apple MacBook Pro 64 GiB 32 gpu, Go can sustainably launch about 240,000 virtual threads
per second until available memory becomes a problem.
The program runs fairly well for 5 minutes and 4 cores up to 50 million threads where progress effectively
stops. Go does not produce an error and the program continues to run, but after a 5-minute swap
session and 233 GiB of acquired memory, Go resigns to a single core and new threads are no longer
launched.

For anyone a former multi-threaded Java programmer, such numbers are
unbelievably fantastic

Go was discovered in 2009 by Rob Pike, Robert Griesemer and Ken Thompson at Google

© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
*/
package main

import (
	"math"
	"os"
	"runtime"
	"strconv"
	"sync"
	"time"

	"codeberg.org/haraldrudell/massivevirtualparallelism/goer"
	"github.com/haraldrudell/parl"
	"github.com/haraldrudell/parl/error116"
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

const (
	uiUpdateFreq              = time.Second
	tickPeriod                = time.Second
	defaultThreadCount uint64 = 2e6
	badUsageStatusCode        = 2
	failStatusCode            = 1
	tickerWait                = 3 * time.Second
	maxUint64Arg              = "-1"
	programName               = "massivevirtualparallelism"
	uiTimeFormat              = "2006-01-02 15:04:05-07:00"
)

var sprintf = message.NewPrinter(language.English).Sprintf // TODO 220321 use parl.Sprintf

var help = "Usage: " + programName + " [thread count]" +
	"\n  no argument: " + sprintf("%d", defaultThreadCount) + " threads" +
	"\n  " + maxUint64Arg + " for 18 trillion (10^18)" +
	"\n  or positive integer number up to 18 trillion" +
	"\nthe hardware might support 50 million in 5 minutes launching 240,000 per second"

func main() {
	start := time.Now()
	var wg sync.WaitGroup
	var errs error116.ParlError
	//parl.Out TODO
	parl.Out("\n%s %s", programName, start.Format(uiTimeFormat))
	memString := goer.MemorySizeString()
	if memString != "" {
		memString += "\x20"
	}
	parl.Out("host: %s %s os-arch: %s %s %scores: %d",
		goer.Hostname(),
		runtime.Version(),
		runtime.GOOS, runtime.GOARCH,
		memString,
		runtime.GOMAXPROCS(0),
	)

	// obtain desired number of threads
	count := parseOptions()

	// launch thread manager and ui update thread
	parl.Log("Launching threads: %d…", count)
	ticker := goer.NewClosingTicker(uiUpdateFreq)
	g := goer.NewGoer(count, tickPeriod, threadFunc, nil) // atomic
	tsOut := goer.TsOut(g.T0)
	wg.Add(1)
	go displayStatusThread(g, ticker.C, tsOut, &wg, &errs)

	tsOut("Status thread count: %d. Waiting for thread launch to complete…", g.StatusThreadCount)
	g.WaitLaunch()
	tsOut("Thread launch complete")

	tsOut("Waiting %s s for ticker events", tickerWait)
	time.Sleep(tickerWait)

	tsOut("Shutting down…")
	g.Shutdown()
	if _, err := g.GetError(); err != nil {
		errs.AddError(err)
	}
	tsOut("Thread shutdown complete")

	// shutdown ui updates
	ticker.Shutdown()
	if err := ticker.ParlError.GetError(); err != nil {
		errs.AddError(parl.Errorf("ticker panic: %w", err))
	}
	wg.Wait()

	// exit
	if err := errs.GetError(); err != nil {
		tsOut(err.Error())
		os.Exit(failStatusCode)
	}
	tsOut(programName + " completed successfully")
}

func threadFunc(g *goer.LaunchData) {
	state := goer.ThreadUpdate{ThreadID: g.ThreadID}
	defer func() { // send thread result on status channel
		state.At = time.Since(g.T0)
		if v := recover(); v != nil {
			state.Recover = v
			state.Status = goer.ThreadPanic
		}
		g.Status <- &state
	}()

	defer close(g.Output)

	g.Status <- &goer.ThreadUpdate{At: time.Since(g.T0), ThreadID: g.ThreadID, Status: goer.ThreadStart}
	for {
		if i, ok := <-g.Input; !ok {
			break // inpout channel closed
		} else {
			g.Output <- i // copy input to output
		}
		state.Count++
	}
	state.Status = goer.ThreadInputClosed
}

func displayStatusThread(g *goer.Goer, C <-chan time.Time, tsOut func(format string, a ...interface{}), wg *sync.WaitGroup, errs *error116.ParlError) {
	defer wg.Done()
	defer func() {
		if v := recover(); v != nil {
			e, ok := v.(error)
			if !ok {
				e = parl.Errorf("Non error value: %v", v)
			}
			errs.AddError(parl.Errorf("status thread panic: %w", e))
		}
	}()

	prior := g.Stats()
	var max goer.GoerMax
	for {
		_, ok := <-C
		if !ok {
			break // ticker was closed: exit
		}

		stats := g.Stats()
		duration := stats.T - prior.T
		if duration > 2*uiUpdateFreq {
			tsOut("EXECUTION DELAY: %s expected: %s",
				goer.FormatD(duration),
				goer.FormatD(uiUpdateFreq))
		}
		tsOut(stats.DiffString(prior))
		max.Update(stats, prior)
		prior = stats
	}
	tsOut(g.String()) // status of g
	maxDuration, _ := g.GetError()
	tsOut("Maximum time between ticks (normally 1 second): %s", goer.FormatD(maxDuration))
	tsOut("Maxima observed: %s", max)
}

func parseOptions() (count uint64) {
	// obtain desired number of threads
	if len(os.Args) < 2 {
		return defaultThreadCount // no argument: default
	}
	if len(os.Args) > 2 { // too many arguments
		parl.Log(help)
		os.Exit(badUsageStatusCode)
	}
	arg := os.Args[1]
	maxUint64 := ^uint64(0) // largest possible uint64
	if arg == maxUint64Arg {
		return maxUint64 // -1 gets M1 Max
	}
	var err error
	if count, err = strconv.ParseUint(arg, 10, 64); err == nil {
		return // sequence of digts that parsed to uint64
	}
	var v float64
	if v, err = strconv.ParseFloat(arg, 64); err == nil {
		if v >= 0 && v <= float64(maxUint64) && math.Floor(v) == v {
			return uint64(v) // some floating point value that fits uint64
		}
	}
	parl.Log("Bad argument: %q\n%s", arg, help)
	os.Exit(badUsageStatusCode)
	return
}
