/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

package goer

import (
	"os"
	"strconv"
	"strings"

	"github.com/haraldrudell/parl"
)

const (
	linuxFile = "/proc/meminfo"
	memTotal  = "MemTotal:"
	kiloByte  = 1024
)

func MemoryLinux() (byts uint64) {
	byteSlice, err := os.ReadFile(linuxFile)
	if parl.IsThisDebug() {
		parl.Debug("os.ReadFile: err: %v result: %q", err, string(byteSlice))
	}
	if err != nil {
		panic(err)
	}
	numberString := strings.TrimLeft(strings.TrimPrefix(strings.Split(string(byteSlice), "\n")[0], memTotal), "\x20")
	parl.Debug("numberString: %s", numberString)
	if index := strings.Index(numberString, "\x20"); index != -1 {
		numberString = numberString[:index]
	}
	parl.Debug("numberString: %s", numberString)
	num, err := strconv.ParseUint(numberString, 10, 64)
	if err != nil {
		return // failed to parse command output
	}
	return num * kiloByte
}
