/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

package goer

import (
	"errors"
	"os/exec"
	"strconv"
	"strings"
)

const (
	macOSCmd       = "memory_pressure"
	memPressPrefix = "The system has "
)

func MemoryMacOS() (byts uint64) {
	byteList, err := exec.Command(macOSCmd).Output()
	if err != nil {
		if errors.Is(err, exec.ErrNotFound) {
			return // the command does not exist
		}
		panic(err)
	}
	numberString := strings.TrimPrefix(strings.Split(string(byteList), "\n")[0], memPressPrefix)
	if index := strings.Index(numberString, "\x20"); index != -1 {
		numberString = numberString[:index]
	}
	num, err := strconv.ParseUint(numberString, 10, 64)
	if err != nil {
		return // failed to parse command output
	}
	return num
}
