/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

package goer

import "time"

type GoerMax struct {
	MaxDuration time.Duration
	MaxValue    uint64
	MaxRate     uint64
}

func (gm *GoerMax) Update(new *GoerStat, old *GoerStat) {
	d := new.T - old.T
	if d > gm.MaxDuration {
		gm.MaxDuration = d
	}
	gm.CheckMax(new.ReceivedTicks, old.ReceivedTicks, d)
	gm.CheckMax(new.Launched, old.Launched, d)
	gm.CheckMax(new.Started, old.Started, d)
	gm.CheckMax(new.Exited, old.Exited, d)
	gm.CheckMax(new.Panic, old.Panic, d)
}

func (gm *GoerMax) CheckMax(new uint64, old uint64, d time.Duration) {
	if new > gm.MaxValue {
		gm.MaxValue = new
	}
	rate := uint64(float32(new-old) / float32(d) * float32(time.Second))
	if rate > gm.MaxRate {
		gm.MaxRate = rate
	}
}

func (gm GoerMax) String() string {
	return sprintf("max duration: %s max value: %d max rate: %d", FormatD(gm.MaxDuration), gm.MaxValue, gm.MaxRate)
}
