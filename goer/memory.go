/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

package goer

import (
	"runtime"
)

const (
	megaByte = 1024 * 1024
	gigaByte = 1024 * megaByte
)

func MemorySize() (bytes uint64) {
	if runtime.GOOS == "darwin" {
		bytes = MemoryMacOS()
	} else if runtime.GOOS == "linux" {
		bytes = MemoryLinux()
	}
	return
}

/*
cgo — does not cross-compile well
func MemorySize() (bytes uint64) {
	pages, pageSize := memorySize()
	return uint64(pages) * uint64(pageSize)
}
*/

func MemorySizeString() (s string) {
	memorySize := MemorySize()
	if memorySize == 0 {
		return
	}
	if memorySize < gigaByte {
		return sprintf("%d MiB", memorySize/megaByte)
	}
	return sprintf("%d GiB", memorySize/gigaByte)
}
