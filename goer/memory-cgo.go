/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

package goer

// #include <unistd.h>
import "C"

// cgo works but does not cross compile without lots of system configuration
func memorySize() (pages uint64, pageSize uint64) {
	pagesC := C.sysconf(C._SC_PHYS_PAGES) // c long: 32 or 64 bits
	pages = uint64(pagesC)
	pageSizeC := C.sysconf(C._SC_PAGE_SIZE) // c long
	pageSize = uint64(pageSizeC)
	return
}
