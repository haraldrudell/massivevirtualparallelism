/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

package goer

import (
	"testing"
	"time"

	"github.com/haraldrudell/parl"
)

func TestGoeR(t *testing.T) {
	var threadCount uint64 = 0
	tickPeriod := time.Second
	shutdownDeadLine := time.Second

	fn := func(goData *LaunchData) {}
	t00 := time.Now()
	g := NewGoer(threadCount, tickPeriod, fn, nil)
	if !g.T0.After(t00) {
		t.Errorf("T0 not after t00")
	}
	if g.ticker.C == nil {
		t.Errorf("g.ticker.C nil")
	}
	shutCh := make(chan struct{})
	go func() {
		parl.D("Invoking Goer.shutdown")
		g.Shutdown()
		parl.D("Goer.shutdown returned")
		close(shutCh)
	}()
	timer := time.NewTimer(shutdownDeadLine)
	select {
	case <-timer.C:
		t.Errorf("shutdown duration over 1 second")
		t.FailNow()
	case <-shutCh:
		timer.Stop()
	}
}

func TestThreadPanic(t *testing.T) {
	var threadCount uint64 = 1
	tickPeriod := time.Second
	fn := func(goData *LaunchData) {
		defer func() {
			goData.Status <- &ThreadUpdate{
				At:       time.Since(goData.T0),
				ThreadID: goData.ThreadID,
				Status:   ThreadPanic,
				Recover:  recover(),
			}
		}()
		defer close(goData.Output)

		panic(1)
	}

	g := NewGoer(threadCount, tickPeriod, fn, nil)
	g.WaitLaunch()
	t.Log("WaitLaunch complete")
	g.Shutdown()
	t.Log("Shutdown complete")

	//t.Error(g.String())
}
