/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

// Package goer manages a large number of virtual threads
package goer

import (
	"math"
	"sync"
	"sync/atomic"
	"time"

	"github.com/haraldrudell/parl"
	"github.com/haraldrudell/parl/error116"
)

const goerThreadsPerChannel = 1e6

// Goer manages threads
type Goer struct {
	T0                time.Time
	Number            uint64
	StatusThreadCount int
	stats             GoerStat       // atomic
	complete          sync.WaitGroup // Goer thread manager interrnal threads
	launchComplete    sync.WaitGroup // launch task complete
	threadsComplete   sync.WaitGroup // managed threads
	shutdownOnce      sync.Once
	error116.ParlError
	ticker *ClosingTicker
	errCh  chan<- error
}

// NewGoer launches a group of managed threads
func NewGoer(threadCount uint64, tickPeriod time.Duration, fn func(goData *LaunchData), errCh chan<- error) (goer *Goer) {
	g := Goer{T0: time.Now(), Number: threadCount,
		ticker: NewClosingTicker(tickPeriod),
		errCh:  errCh,
	}

	// g.launchComplete waits for all threads to have launched
	g.launchComplete.Add(1)

	// status messages are sent twice by all threads
	// sending a pointer on a channel takes about 625 ns, ie. 1.6 million per second
	// status thread executes a handful of statements per received pointer, maybe 1.25 μs
	// 1 stati thread per 1 million threads processes 1 message per thread in 1.25 s
	// observed rates for all threads are around 1 million per second
	statiCount := threadCount / goerThreadsPerChannel
	if statiCount == 0 {
		statiCount = 1
	} else if statiCount > math.MaxInt {
		statiCount = math.MaxInt
	}
	count := int(statiCount)
	g.StatusThreadCount = count
	statiChannels := make([]chan<- *ThreadUpdate, count)
	g.complete.Add(count)
	for n := 0; n < count; n++ {
		statusChannel := make(chan *ThreadUpdate)
		statiChannels[n] = statusChannel
		go g.consumeStati(statusChannel)
	}

	g.complete.Add(1)
	go g.launchThreads(fn, statiChannels) // goroutine launcher

	return &g
}

// WaitLaunch wait untiul all threads have been launched
func (goer *Goer) WaitLaunch() {
	goer.launchComplete.Wait()
}

// Stats returns the current status of the managed threads
func (goer *Goer) Stats() (gs *GoerStat) {
	gs0 := &goer.stats
	return &GoerStat{
		T:             time.Since(goer.T0),
		ReceivedTicks: atomic.LoadUint64(&gs0.ReceivedTicks),
		Launched:      atomic.LoadUint64(&gs0.Launched),
		Started:       atomic.LoadUint64(&gs0.Started),
		Exited:        atomic.LoadUint64(&gs0.Exited),
		Panic:         atomic.LoadUint64(&gs0.Panic),
		Err:           goer.ParlError.GetError(), // parl 0.1.0
	}
}

// String produces a string representation of the currently running threads
func (goer *Goer) String() string {
	return goer.Stats().StringState()
}

// Shutdown shuts down all managed threads and the Goer instance
func (goer *Goer) Shutdown() {
	goer.shutdownOnce.Do(func() {
		goer.ticker.Shutdown()
		if _, err := goer.ticker.GetError(); err != nil {
			goer.addError(err)
		}
		goer.complete.Wait() // wait for launchThreads consumeStati
		if goer.errCh != nil {
			close(goer.errCh)
		}
	})
}

func (goer *Goer) GetError() (maxDuration time.Duration, err error) {
	maxDuration, _ = goer.ticker.GetError()
	err = goer.ParlError.GetError()
	return
}

func (goer *Goer) launchThreads(fn func(goData *LaunchData), statiChannels []chan<- *ThreadUpdate) {
	defer goer.complete.Done()
	defer goer.catchPanic("launchThreads")
	defer func() {
		for _, ch := range statiChannels {
			close(ch)
		}
	}()

	var threadID uint64
	launchP := &goer.stats.Launched
	var launched uint64
	input := goer.ticker.C
	for launched < goer.Number {
		output := make(chan time.Time)
		statiChannel := statiChannels[int(launched%uint64(len(statiChannels)))]
		goer.threadsComplete.Add(1)
		go fn(&LaunchData{
			T0:       goer.T0,
			ThreadID: threadID,
			Input:    input,
			Output:   output,
			Status:   statiChannel,
		})
		input = output
		threadID++
		launched = atomic.AddUint64(launchP, 1)
	}
	goer.launchComplete.Done()

	// now listen to that last channel
	rxp := &goer.stats.ReceivedTicks
	for {
		_, ok := <-input
		if !ok {
			break // the last goroutine closed the channel
		}
		atomic.AddUint64(rxp, 1)
	}

	// signal to managed threads to shutdown
	goer.ticker.Shutdown()
	goer.threadsComplete.Wait()
}

func (goer *Goer) catchPanic(threadName string) {
	if v := recover(); v != nil {
		goer.addError(
			parl.Errorf("thread: %s: %w", threadName,
				parl.EnsureError(v)))
	}
}

func (goer *Goer) addError(err error) {
	goer.AddError(err) // parl 0.1.0
	if goer.errCh != nil {
		goer.errCh <- err
	}
}

func (goer *Goer) consumeStati(stati <-chan *ThreadUpdate) {
	defer goer.complete.Done()
	defer goer.catchPanic("consumeStati")

	for {
		update, ok := <-stati
		if !ok {
			break
		}
		switch update.Status {
		case ThreadStart:
			atomic.AddUint64(&goer.stats.Started, 1)
		case ThreadPanic:
			atomic.AddUint64(&goer.stats.Panic, 1)
			goer.addError(parl.EnsureError(update.Recover))
			goer.threadsComplete.Done()
		case ThreadInputClosed:
			atomic.AddUint64(&goer.stats.Exited, 1)
			goer.threadsComplete.Done()
		default:
			goer.addError(parl.Errorf("Bad status: %d from id: %d", update.Status, update.ThreadID))
		}
	}
}
