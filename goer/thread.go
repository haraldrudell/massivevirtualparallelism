/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

package goer

import (
	"time"
)

const (
	ThreadInvalid     Status = iota // bad: thread sent uninitialzied struct
	ThreadStart                     // thread has started reading its input channel
	ThreadInputClosed               // thread normal exit from input channel close
	ThreadPanic                     // thread terminated with panic
)

type Status uint8

// LaunchData is what a thread receives as launch parameter
type LaunchData struct {
	T0       time.Time
	ThreadID uint64
	Input    <-chan time.Time
	Output   chan<- time.Time
	Status   chan<- *ThreadUpdate
}

// ThreadUpdate is what a thread sends back on exit on the Status channel
type ThreadUpdate struct {
	At       time.Duration
	ThreadID uint64
	Status   Status
	Count    uint64
	Recover  interface{}
}
