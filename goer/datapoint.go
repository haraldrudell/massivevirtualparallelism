/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

package goer

import (
	"golang.org/x/text/language"
	"golang.org/x/text/message"
)

type DataPoint struct {
	Label      string
	IsNegative bool
	Value      uint64
}

var sprintf = message.NewPrinter(language.English).Sprintf

func NewDataPoint(label string, oldValue, newValue uint64) (d *DataPoint) {
	d0 := DataPoint{Label: label}
	d0.IsNegative = newValue < oldValue
	if d0.IsNegative {
		d0.Value = oldValue - newValue
	} else {
		d0.Value = newValue - oldValue
	}
	return &d0
}

func (dp *DataPoint) IsSame() bool {
	return dp.Value == 0
}

func (dp *DataPoint) String() string {
	s := ""
	if dp.IsNegative {
		s = "-'"
	}
	return sprintf("%s %s%d",
		dp.Label,
		s,
		dp.Value,
	)
}
