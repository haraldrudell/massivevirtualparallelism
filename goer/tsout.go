/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

package goer

import (
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/haraldrudell/parl"
)

// TsOut returns a printf logging function that prepends timestamp relative to T0
func TsOut(T0 time.Time) (fn func(format string, a ...interface{})) {

	// logging function that prepends timestamp relative to T0
	return func(format string, a ...interface{}) {
		if len(a) == 0 {
			a = []interface{}{format}
			format = "%s"
		}
		timeStamp := FormatD(time.Since(T0))
		a = append([]interface{}{timeStamp}, a...)
		parl.Log("%s "+format, a...)
	}
}

// FormatD converts time.Duration to string with fewer digits
func FormatD(d time.Duration) (s string) {
	if d < time.Microsecond {
		return fmt.Sprintf("%dns", d)
	} else if d < time.Millisecond {
		return fmt.Sprintf("%dμs", d/time.Microsecond)
	} else if d < time.Second {
		return fmt.Sprintf("%dms", d/time.Millisecond)
	} else if d < time.Minute {
		return fmt.Sprintf("%.1fs", float32(d)/float32(time.Second))
	}
	return d.Round(time.Second).String()
}

func Hostname() (hostname string) {
	var err error
	if hostname, err = os.Hostname(); err != nil {
		panic(parl.Errorf("os.Hostname: %w", err))
	}
	if index := strings.Index(hostname, "."); index != -1 {
		hostname = hostname[:index]
	}
	return
}
