/*
© 2022–present Harald Rudell <harald.rudell@gmail.com> (https://haraldrudell.github.io/haraldrudell/)
ISC License
*/

package goer

import (
	"strings"
	"time"
)

// GoerStats contains the current execution state of managed threads
type GoerStat struct {
	T             time.Duration
	ReceivedTicks uint64
	Launched      uint64
	Started       uint64
	Exited        uint64
	Panic         uint64
	Err           error
}

func (gs *GoerStat) DiffString(prior *GoerStat) string {
	sList := []string{sprintf("running: %d", gs.Launched-gs.Exited-gs.Panic)}
	value := func(label, rateLabel string, newer, older uint64) {
		if newer == older {
			return
		}
		sList = append(sList, sprintf("%s: %d", label, newer))
		if rateLabel == "" {
			return
		}
		sList = append(sList, sprintf("%s: %s", rateLabel, gs.Rate(prior.T, newer, older)))
	}
	value("ticks", "", gs.ReceivedTicks, prior.ReceivedTicks)
	value("launched", "launch-rate", gs.Launched, prior.Launched)
	value("started", "start-rate", gs.Started, prior.Started)
	value("exited", "exit-rate", gs.Exited, prior.Exited)
	value("panic", "", gs.Panic, prior.Panic)
	err := gs.Err
	if err == nil && prior.Err != nil {
		err = prior.Err
	}
	if err != nil {
		sList = append(sList, gs.Err.Error())
	}
	if len(sList) == 0 {
		return gs.String()
	}
	return strings.Join(sList, "\x20")
}

func (gs *GoerStat) Rate(T time.Duration, newer, older uint64) string {
	rateFloat := float32(newer-older) / float32(gs.T-T) * float32(time.Second)
	return sprintf("%.0f/s", rateFloat)
}

func (gs *GoerStat) StringState() string {
	errs := "none"
	if gs.Err != nil {
		errs = gs.Err.Error()
	}
	return sprintf(
		"running: %d ticks: %d launched: %d"+
			" started: %d exited: %d panic: %d errs: %s",
		gs.Launched-gs.Exited-gs.Panic,
		gs.ReceivedTicks,
		gs.Launched,
		gs.Started,
		gs.Exited,
		gs.Panic,
		errs,
	)
}

// String produces a string representation of the currently running threads
func (gs *GoerStat) String() string {
	return sprintf("%s %s", FormatD(gs.T), gs.StringState())
}
